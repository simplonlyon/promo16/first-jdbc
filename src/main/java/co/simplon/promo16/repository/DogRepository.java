package co.simplon.promo16.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.promo16.entity.Dog;

public class DogRepository implements IDogRepository {

    @Override
    public List<Dog> findAll() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/first_jdbc");
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog");
            ResultSet result = stmt.executeQuery();
            List<Dog> dogList = new ArrayList<>();
            while (result.next()) {
                Dog dog = new Dog(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("breed"),
                        result.getDate("birthdate").toLocalDate());
                dogList.add(dog);
            }
            return dogList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Dog findById(Integer id) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/first_jdbc");
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog WHERE id=?");
            stmt.setInt(1, id);
            //TODO: le reste

        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void save(Dog dog) {
        // TODO Auto-generated method stub

    }

    @Override
    public void update(Dog dog) {
        // TODO Auto-generated method stub

    }

    @Override
    public void deleteById(Integer id) {
        // TODO Auto-generated method stub

    }

}
