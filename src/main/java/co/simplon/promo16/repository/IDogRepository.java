package co.simplon.promo16.repository;

import java.util.List;

import co.simplon.promo16.entity.Dog;
/**
 * Classe représentant un Repository/DAO assez basique avec les méthodes du
 * CRUD les plus utilisées
 */
public interface IDogRepository {
    /**
     * Récupère tous les chiens en base de données et les convertis en entity Dog
     * @return Une liste d'instance de Dog représentant les chiens contenus en BDD
     */
    List<Dog> findAll();
    /**
     * Récupère un chien spécifique en se basant sur son id
     * @param id L'id du chien à récupérer
     * @return Une instance de Dog ou null si aucun chien n'a cet id
     */
    Dog findById(Integer id);
    /**
     * Méthode permettant de faire persister un chien en base de données.
     * Son but sera de prendre une instance de chien et de la convertir en une
     * requête SQL (INSERT INTO)
     * @param dog L'instance de Dog à faire persister en base de données
     */
    void save(Dog dog);
    /**
     * Méthode qui permet de mettre à jour un chien déjà en base de données
     * (cette méthode n'existera pas vraiment dans les repository Spring
     * car le update est fait aussi à partir du save, mais on la met pour plus
     * de simplicité au début)
     * @param dog Le chien qu'on souhaite mettre à jour en base de données
     */
    void update(Dog dog);
    /**
     * Méthode qui permet de supprimer un chien persisté en base de données en se
     * basant sur son id
     * @param id l'id du chien à supprimer
     */
    void deleteById(Integer id);
}
