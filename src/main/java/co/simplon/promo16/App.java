package co.simplon.promo16;

import co.simplon.promo16.repository.DogRepository;
import co.simplon.promo16.repository.IDogRepository;


public class App 
{
    public static void main( String[] args )
    {
        IDogRepository repo = new DogRepository();
        System.out.println(repo.findAll());
    }
}
