DROP TABLE IF EXISTS dog;

CREATE TABLE dog(
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    breed VARCHAR(60),
    name VARCHAR(170) NOT NULL,
    birthdate DATE
);

INSERT INTO dog (name,breed,birthdate) VALUES ("Fido", "corgi", "2021-12-01"), 
                                                ("Max", "Shiba", "2020-10-01");
