# First JDBC
Un projet où on fait des DAO/Repository avec JDBC

(Une version de l'entité avec un uuid plutôt qu'un id auto increment [sur cette branche](https://gitlab.com/simplonlyon/promo16/first-jdbc/-/tree/uuid) )

## How to Use
1. Créer une base de données `first_jdbc`
2. Importer le fichier [database.sql](database.sql) dans votre base de données, soit via l'extension (clic droit sur la bdd first_jdbc > import sql), soit via le terminal `mysql -u simplon -p first_jdbc < database.sql`

Il est nécessaire d'avoir un user simplon avec le mot de passe 1234 pour que ça fonctionne (sinon changer la string de connexion dans le DriverManager.getConnection)


## Exercices

### Faire un Repository
```java
package co.simplon.promo16;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/first_jdbc");
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog");
            ResultSet result = stmt.executeQuery();
            result.next();
            System.out.println(result.getInt("id"));
            System.out.println(result.getString("name"));
            System.out.println(result.getString("breed"));
            result.next();
            System.out.println(result.getInt("id"));
            System.out.println(result.getString("name"));
            System.out.println(result.getString("breed"));
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
    }
}
```
1. Modifier le code ci-dessus pour faire en sorte de rajouter une boucle while qui tournera tant que next()
2. à l'intérieur de cette boucle, placer les sysout des result.get pour afficher les différents résultat de la requête
3. Ensuite, retirer les sysout et à la place, créer une instance de la classe Dog en lui donnant, via le constructeur ou via les setter, les différentes informations (pour l'instant on peut ignorer la birthdate)
4. Créer une List<Dog> au dessus de la boucle, et dans la boucle, mettre l'instance dans la liste
5. Créer une classe DogRepository dans le package repository et lui créer une méthode findAll() qui renverra une List<Dog> et dans cette méthode, placer tout le code fait jusque là en faisant un return de la liste de chien

### Terminer le Repository (depuis le findAll et le début de findById)
1. Pour le findById, il va falloir faire presque comme dans le findAll, sauf qu'au lieu de faire une liste et un while, on va pouvoir juste faire un if sur le next() et un return du new dog à l'intérieur
2. Comme on aime pas trop la répétition, on va faire en sorte d'externaliser le getConnection, plusieurs possibilités, on peut par exemple faire une propriété Connection sur notre DogRepository et faire le getConnection dans un constructeur (on peut aussi faire une fonction "connect" qu'on appellera dans chaque requête)
3. On peut éventuellement faire le delete ici, qui ressemblera beaucoup au findById, sauf qu'on aura pas besoin de ResultSet et de next dedans, juste d'exécuter la requête (solution possible sur [cette branche](https://gitlab.com/simplonlyon/promo16/first-jdbc/-/tree/findById) )
4. Pour le save, le début va être pareil mais cette fois on aura une requête INSERT INTO (si vous êtes pas sûr·e d'à quoi elle devrait ressembler, faite la d'abord dans mysql directement avant de la copier/coller dans le Java), et on remplacera chaque valeur par un "?" dans la requête, et ensuite on fera des set... pour chaque "?" en y assignant les valeurs du Dog (on aura pas forcément besoin du ResultSet pour cette requête non plus)
5. Même principe pour le update ([solution possible](https://gitlab.com/simplonlyon/promo16/first-jdbc/-/tree/save-update))